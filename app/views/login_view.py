from os import access
from werkzeug.wrappers import request
from app.models.user_model import UserModel
from flask import  request,Blueprint 
from http import HTTPStatus
from flask_jwt_extended import create_access_token

bp_login = Blueprint('bp_login', __name__)


@bp_login.route("/signin", methods=["POST"])
def login():
    data = request.json
    found_user: UserModel = UserModel.query.filter_by(email=data['email']).first()

    if not found_user:
        return {"message": "User not found"}, HTTPStatus.NOT_FOUND

    if found_user.verify_password(data["password"]):
        access_token = create_access_token(identity=found_user)
        return {"message": access_token}, HTTPStatus.OK
    else:
        return {"message": "Unauthorized"}, HTTPStatus.UNAUTHORIZED


