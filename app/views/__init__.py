from flask import Flask
from .user_view import bp_user
from .login_view import bp_login

def init_app(app: Flask):
    app.register_blueprint(bp_user)
    app.register_blueprint(bp_login)