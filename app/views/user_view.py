import re
from sqlalchemy.orm import query
from werkzeug.wrappers import request
from app.models.user_model import UserModel
from flask import json, jsonify, request, current_app, Blueprint 
from http import HTTPStatus
from secrets import token_urlsafe
from app.configs.auth import auth

bp_user = Blueprint('user', __name__)


@bp_user.route('/signup', methods=["POST"])
def create_user():
    data = request.json
    # password_to_hash = data.pop("password")
    data['api_key'] = token_urlsafe(16)
    print(data)
    user = UserModel(**data)
    print(user)
    # user.password = password_to_hash
    current_app.db.session.add(user)
    current_app.db.session.commit()

    return jsonify(user), HTTPStatus.CREATED


@bp_user.route('/', methods=["GET"])
@auth.login_required
def get_user():

    try:
        auth = request.headers.get('Authorization').split(' ')[1]
        user = UserModel.query.filter_by(api_key=auth).first()
        return jsonify(user), 200
    
    except:
        return {"message": "Unauthorized"}, HTTPStatus.UNAUTHORIZED


@bp_user.route('/', methods=["PUT"])
@auth.login_required
def update():
    data = request.get_json()
    auth = request.headers.get('Authorization').split(' ')[1]
    user = UserModel.query.filter_by(api_key=auth).first()
    for key, value in data.items():
        setattr(user, key, value)

    current_app.db.session.add(user)
    current_app.db.session.commit()
    

    result = {
        "name": user.name,
        "last_name": user.last_name,
        "email": user.email
    }

    return result, 200

@bp_user.route('/', methods=["DELETE"])
@auth.login_required
def delete():
    auth = request.headers.get('Authorization').split(' ')[1]
    user = UserModel.query.filter_by(api_key=auth).first()
    current_app.db.session.delete(user)
    current_app.db.session.commit()

    return {'msg': f'{user.name} has been deleted'}, 200
