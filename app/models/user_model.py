from enum import unique
from re import T
from typing import Text
from sqlalchemy.sql.sqltypes import Integer
from app.configs.database import db 
from dataclasses import dataclass
from sqlalchemy import String, Column
from werkzeug.security import generate_password_hash, check_password_hash


@dataclass
class UserModel(db.Model):

    name: str
    last_name: str
    email: str


    __tablename__ = 'user_table'


    id = Column(Integer, primary_key=True, nullable=False)
    name = Column(String(127), nullable=False)
    last_name = Column(String(511), nullable=False)
    email = Column(String(255), nullable=False, unique=True)
    password_hash = Column(String(511), nullable=True)

    @property
    def password(self):
        raise AttributeError("Password cannot be acessed")

    @password.setter
    def password(self, password_to_hash):
        self.password_hash = generate_password_hash(password_to_hash)

    def verify_password(self, password_to_compare):
        return check_password_hash(self.password_hash, password_to_compare)

    
    def update(self, data):
        self.name = data['name']
        self.last_name = data['last_name'] 
        self.email = data['email']
        self.password = data['password']